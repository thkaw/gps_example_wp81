﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.Geolocation;
using Windows.UI.Core;
using System.Threading.Tasks;
using Windows.System.Threading;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/p/?LinkID=234238

namespace gps_example_w81
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Geolocator geo = null;
        private CoreDispatcher _cd;
        private double prevLatitude = -1;
        private double prevLongitude = -1;
        private double totalDistance = 0;

        private DateTimeOffset StartTime, prevTime, currentTime;
        private double userWeight = 0;

        bool PositionActive = false;
        PositionChangedEventArgs old_e;

        DispatcherTimer timer;
        public MainPage()
        {
            this.InitializeComponent();
            _cd = Window.Current.CoreWindow.Dispatcher;
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (geo == null)
            {
                geo = new Geolocator();
            }
            if (geo != null)
            {
                geo.MovementThreshold = float.Parse(tbThreshold.Text);
                userWeight = double.Parse(tb_Weight.Text);

                geo.DesiredAccuracy = PositionAccuracy.High;
                geo.DesiredAccuracyInMeters =  uint.Parse(tb_AccuracyInMeters.Text);
                geo.ReportInterval = uint.Parse(tb_ReportInterval.Text); //目前3秒抓一次gps 



                geo.PositionChanged += new TypedEventHandler<Geolocator,
                    PositionChangedEventArgs>(geo_PositionChanged);
                geo.StatusChanged += new TypedEventHandler<Geolocator,
                    StatusChangedEventArgs>(geo_StatusChanged);

                tbThreshold.IsEnabled = false;
                TextBox1.Text = "Tracking Started " +
                                "(Time, Latitude, Longitude, Distance)\n";

                StartTime = DateTime.Now;

                timer = new DispatcherTimer();
                timer.Tick += timer_Tick;
                timer.Interval = new TimeSpan(00, 0, 4);
                bool enabled = timer.IsEnabled;
                timer.Start();

            }
        }

        void timer_Tick(object sender, object e)
        {
            if (!PositionActive)
            {
                UpdatePos(old_e); //如果40秒沒換位置，強制做一次update
            }
            else
            {
                PositionActive = false;
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (geo != null)
            {
                geo.PositionChanged -= new TypedEventHandler<Geolocator,
                    PositionChangedEventArgs>(geo_PositionChanged);
                geo.StatusChanged -= new TypedEventHandler<Geolocator,
                    StatusChangedEventArgs>(geo_StatusChanged);
                TextBox1.Text += "\nTracking Stopped.\n" +
                                 "Total Distance recorded: " +
                                 totalDistance.ToString("F2") + " m\n";
                tbThreshold.IsEnabled = true;
                timer.Stop();
            }
        }

        private double CalculateDistance(double prevLat, double prevLong, double currLat, double currLong)
        {
            const double degreesToRadians = (Math.PI / 180.0);
            const double earthRadius = 6371; // kilometers

            // convert latitude and longitude values to radians
            var prevRadLat = prevLat * degreesToRadians;
            var prevRadLong = prevLong * degreesToRadians;
            var currRadLat = currLat * degreesToRadians;
            var currRadLong = currLong * degreesToRadians;

            // calculate radian delta between each position.
            var radDeltaLat = currRadLat - prevRadLat;
            var radDeltaLong = currRadLong - prevRadLong;

            // calculate distance
            var expr1 = (Math.Sin(radDeltaLat / 2.0) *
                         Math.Sin(radDeltaLat / 2.0)) +

                        (Math.Cos(prevRadLat) *
                         Math.Cos(currRadLat) *
                         Math.Sin(radDeltaLong / 2.0) *
                         Math.Sin(radDeltaLong / 2.0));

            var expr2 = 2.0 * Math.Atan2(Math.Sqrt(expr1),
                                         Math.Sqrt(1 - expr1));

            var distance = (earthRadius * expr2);
            return distance * 1000;  // return results as meters
        }

        async private void geo_PositionChanged(Geolocator sender, PositionChangedEventArgs e)
        {
            await _cd.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                timer.Stop();
                UpdatePos(e);
                timer.Start();
            });
        }

        private async Task UpdatePos(PositionChangedEventArgs e)
        {
            old_e = e;
            PositionActive = true;
            // take this section.
            //Geoposition pos = await geo.GetGeopositionAsync();

            Geoposition pos = e.Position;
            double updateDistance = 0;

            // Calculate distance;
            if ((prevLatitude == -1) || (prevLongitude == -1))
            {
                updateDistance = 0;
            }
            else
            {
                updateDistance = CalculateDistance(prevLatitude, prevLongitude,
                   pos.Coordinate.Point.Position.Latitude, pos.Coordinate.Point.Position.Longitude);

                if (updateDistance < 1)
                { updateDistance = 0; }


                // calculate speed.
                double KM = updateDistance / 1000; //meter
                currentTime = pos.Coordinate.Timestamp;
                TimeSpan diff = currentTime - prevTime;

                double hrs = diff.TotalHours;
                double speed = KM / hrs;

                if (speed == double.NaN || speed < 1)
                { speed = 0; }

                tb_Speed.Text = speed.ToString("F2");


                totalDistance += updateDistance;

                //calculate avg speed.
                double AVGKM = (totalDistance / 1000);
                TimeSpan diffAvg = currentTime - StartTime;
                double hrsAvg = diffAvg.TotalHours;
                double AVGspeed = AVGKM / hrsAvg;
                tb_avgSpeed.Text = AVGspeed.ToString("F2");

                //calculate calories.
                double calories = userWeight * 10 * diffAvg.TotalHours;
                tb_Cal.Text = calories.ToString("F2");



            }

            // Update tracking
            prevLatitude = pos.Coordinate.Point.Position.Latitude;
            prevLongitude = pos.Coordinate.Point.Position.Longitude;
            prevTime = pos.Coordinate.Timestamp;





            // display the results.
            TextBox1.Text += "Position Update: " +
                             pos.Coordinate.Timestamp.ToString("T") + ", " +
                             pos.Coordinate.Point.Position.Latitude.ToString("F3") + ", " +
                             pos.Coordinate.Point.Position.Longitude.ToString("F3") + ", " +
                             updateDistance.ToString("F2") + " m\n";
        }

        async private void geo_StatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            var newStatus = args.Status;
            var strStatus = "";

            switch (newStatus)
            {
                case PositionStatus.Ready:
                    strStatus = "Location is available.";
                    break;
                case PositionStatus.Initializing:
                    strStatus = "Geolocation service is initializing.";
                    break;
                case PositionStatus.NoData:
                    strStatus = "Location service data is not available.";
                    break;

                case PositionStatus.Disabled:
                    strStatus = "Location services are disabled. Use the " +
                                "Settings charm to enable them.";
                    break;

                case PositionStatus.NotInitialized:
                    strStatus = "Location status is not initialized because " +
                                "the app has not yet requested location data.";
                    break;

                case PositionStatus.NotAvailable:
                    strStatus = "Location services are not supported on your system.";
                    break;

                default:
                    strStatus = "Unknown PositionStatus value (" +
                                newStatus.ToString() + ").";
                    break;
            }

            await _cd.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                TextBox1.Text += strStatus + "\n";
            });

        }
        Geoposition posA, posB;
        private async void btn_forcePos_Click(object sender, RoutedEventArgs e)
        {
            posA = await geo.GetGeopositionAsync();
            TextBox1.Text += "posA:" + posA.Coordinate.Latitude + "," + posA.Coordinate.Longitude + "\n";
        }

        private async void btn_forcePosAnother_Click(object sender, RoutedEventArgs e)
        {
            posB = await geo.GetGeopositionAsync();
            TextBox1.Text += "posB:" + posB.Coordinate.Latitude + "," + posB.Coordinate.Longitude + "\n";
        }

        private async void btn_tick_Click(object sender, RoutedEventArgs e)
        {
            await UpdatePos(null);
        }
    }
}

